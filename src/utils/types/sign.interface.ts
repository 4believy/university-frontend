export interface ISignRequest {
  email: string;
  password: string;
}

export interface ISignResponse {
  accessToken: string;
}
