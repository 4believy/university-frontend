import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { ISignResponse } from '../../utils/types/sign.interface';
import { setToken } from '../../api/auth';
import * as authOperations from './authOperations';

export interface IAuthInitialState {
  accessToken: null | string;
  isSuccess: boolean;
  isError: any;
}

const initialState: IAuthInitialState = {
  accessToken: null,
  isSuccess: false,
  isError: null,
};

export const authSlice = createSlice({
  name: 'auth',

  initialState,
  reducers: {
    logOut: (store) => {
      store.accessToken = null;

      setToken('');
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(authOperations.signUp.pending, (state) => {
        state.isError = null;
        state.isSuccess = false;
      })

      .addCase(authOperations.signUp.fulfilled, (state) => {
        state.isSuccess = true;
      })

      .addCase(
        authOperations.signUp.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isError = payload;
          state.isSuccess = false;
        },
      )

      .addCase(authOperations.signIn.pending, (state) => {
        state.isError = null;
      })

      .addCase(
        authOperations.signIn.fulfilled,
        (state, { payload }: PayloadAction<ISignResponse>) => {
          state.accessToken = payload.accessToken;
        },
      )

      .addCase(
        authOperations.signIn.rejected,
        (state, { payload }: PayloadAction<string | undefined>) => {
          state.isError = payload;
        },
      );
  },
});

export const { logOut } = authSlice.actions;

export default authSlice.reducer;
