import AuthPagesSnippet from '../../components/headers/AuthHeader/AuthHeader';
import SignInForm from '../../components/forms/AuthForms/SignInForm';
import LinkComponent from '../../components/common/LinkComponent/LinkComponent';

const SignIn = () => {
  return (
    <AuthPagesSnippet title="welcome">
      <SignInForm></SignInForm>
      <LinkComponent navigateTo="/sign-up">Sign up</LinkComponent>
    </AuthPagesSnippet>
  );
};

export default SignIn;
