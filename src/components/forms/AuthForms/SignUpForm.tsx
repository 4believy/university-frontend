import { useState } from 'react';

import Button from '../../common/Button/Button';
import Checkbox from '../../common/Checkbox/Checkbox';
import styles from './AuthForms.module.scss';
import FormInput from '../../common/FormInput/FormInput';
import { AppDispatchType } from '../../../redux/store';
import { useDispatch } from 'react-redux';
import * as authOperations from '../../../redux/slices/authOperations';
import { useNavigate } from 'react-router-dom';

const SignUpForm = () => {
  const [values, setValues] = useState({
    email: '',
    password: '',
    confirmpassword: '',
  });
  const [isShown, setIsSHown] = useState(false);
  const togglePassword = () => {
    setIsSHown((isShown) => !isShown);
  };
  const navigate = useNavigate();

  const onChange = (e: any) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const dispatch: AppDispatchType = useDispatch();

  const onSubmit = (e: any): void => {
    e.preventDefault();

    const { email, password } = values;

    dispatch(authOperations.signUp({ email, password }));

    navigate('/sign-in');
  };

  return (
    <>
      <form noValidate className={styles.authForm} onSubmit={onSubmit}>
        <FormInput
          id="email"
          type="email"
          label="Email"
          name="email"
          value={values.email}
          onChange={(e: any) => onChange(e)}
          placeholder="name@mail.com"
        ></FormInput>

        <FormInput
          id="password"
          type={isShown ? 'text' : 'password'}
          label="Password"
          name="password"
          value={values.password}
          onChange={(e: any) => onChange(e)}
        ></FormInput>

        <FormInput
          id="password"
          type={isShown ? 'text' : 'password'}
          label="Password"
          name="confirmpassword"
          value={values.confirmpassword}
          onChange={(e: any) => onChange(e)}
        ></FormInput>

        <Checkbox
          id="checkbox"
          label="Show Password"
          checked={isShown}
          onChange={togglePassword}
        ></Checkbox>

        <Button type="submit">Register</Button>
      </form>
    </>
  );
};

export default SignUpForm;
