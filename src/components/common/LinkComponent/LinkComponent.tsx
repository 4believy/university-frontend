import { Link } from 'react-router-dom';
import styles from './LinkComponent.module.scss';

interface Props {
  navigateTo: string;
  children: string;
  design?: 'primary' | 'secondary';
}

const LinkComponent = ({ navigateTo, children, design = 'primary' }: Props) => {
  return (
    <div
      className={
        design === 'secondary'
          ? `${styles.link__wrapper} ${styles.link__wrapper_violet}`
          : `${styles.link__wrapper}`
      }
    >
      <Link className={styles.link} to={navigateTo}>
        {children}
      </Link>
    </div>
  );
};

export default LinkComponent;
