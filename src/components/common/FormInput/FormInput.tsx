import React, { InputHTMLAttributes } from 'react';
import styles from './FormInput.module.scss';

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  id: string;
  type: string;
  name: string;
  label: string;
  value: string;
  placeholder?: string;
  onChange?: any;
  error?: string | undefined;
}

const FormInput = ({
  id,
  type,
  name,
  label,
  value,
  placeholder,
  onChange,
  error,
}: Props) => {
  return (
    <div className={`${styles.inputBlock} ${error && styles.inputBlock_error}`}>
      <label htmlFor={id}>{label}</label>
      <input
        id={id}
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
      />
      {error && <span>{error}</span>}
    </div>
  );
};

export default FormInput;
