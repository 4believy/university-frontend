import styles from './DashboardButton.module.scss';
import dashboardPlus from '../../../assets/icons/plusIcon.svg';

interface Props {
  children: string;
  onClick?: () => void;
}

const DashboardButton = ({ children, onClick }: Props) => {
  return (
    <button className={styles.DashButton} type="button" onClick={onClick}>
      <img className={styles.DashButton__icon} src={dashboardPlus} alt="" />
      <p className={styles.DashButton__text}>{children}</p>
    </button>
  );
};

export default DashboardButton;
