import styles from './Button.module.scss';

interface Props {
  type?: 'button' | 'reset' | 'submit';
  children: string;
  onClick?: () => void;
}
const Button = ({ children, type, onClick }: Props) => {
  return (
    <button className={styles.button} type={type} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
