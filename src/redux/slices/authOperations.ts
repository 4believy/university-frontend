import { createAsyncThunk } from '@reduxjs/toolkit';
import * as authApi from '../../api/auth';
import { AxiosResponse } from 'axios';
import { toast } from 'react-toastify';
import { ISignRequest } from '../../utils/types/sign.interface';
import { ISignResponse } from '../../utils/types/sign.interface';

export const signUp = createAsyncThunk<
  ISignResponse,
  ISignRequest,
  { rejectValue: string }
>('auth/sign-up', async (signUpFormData, { rejectWithValue }) => {
  try {
    const { data }: AxiosResponse<ISignResponse> = await authApi.signUp(
      signUpFormData,
    );

    toast.success('Registration was successful');

    return data;
  } catch (error: any) {
    const errorMessage = error.response?.data.message;

    toast.error(errorMessage);

    return rejectWithValue(errorMessage);
  }
});

export const signIn = createAsyncThunk<
  ISignResponse,
  ISignRequest,
  { rejectValue: string }
>('auth/sign-in', async (signInFormData) => {
  const { data }: AxiosResponse<ISignResponse> = await authApi.signIn(
    signInFormData,
  );

  toast.success('Logged in successfully');

  return data;
});
