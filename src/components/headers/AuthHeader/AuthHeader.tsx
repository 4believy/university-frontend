import { ReactNode } from 'react';
import logo from '../../../assets/icons/logo.svg';
import styles from './AuthHeader.module.scss';

interface Props {
  title: string;
  children: ReactNode;
}

const AuthHeader = ({ title, children }: Props) => {
  return (
    <div className={styles.AuthHeader}>
      <img className={styles.AuthHeader__logo} src={logo} alt="logo" />
      <h6 className={styles.AuthHeader__title}>{title}</h6>
      {children}
    </div>
  );
};

export default AuthHeader;
