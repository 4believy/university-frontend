import { useState } from 'react';
import Button from '../../common/Button/Button';
import Checkbox from '../../common/Checkbox/Checkbox';
import styles from './AuthForms.module.scss';
import * as authOperations from '../../../redux/slices/authOperations';
import FormInput from '../../common/FormInput/FormInput';
import { AppDispatchType } from '../../../redux/store';
import { useDispatch } from 'react-redux';

const SignInForm = () => {
  const [values, setValues] = useState({
    email: '',
    password: '',
  });
  const [isShown, setIsSHown] = useState(false);

  const onChange = (e: any) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const togglePassword = () => {
    setIsSHown((isShown) => !isShown);
  };

  const dispatch: AppDispatchType = useDispatch();

  const onSubmit = (e: any): void => {
    e.preventDefault();
    const { email, password } = values;

    dispatch(authOperations.signIn({ email, password }));
  };

  return (
    <>
      <form className={styles.authForm} onSubmit={onSubmit}>
        <FormInput
          id="email"
          type="email"
          label="Email"
          name="email"
          value={values.email}
          onChange={(e: any) => onChange(e)}
          placeholder="name@mail.com"
        ></FormInput>

        <FormInput
          id="password"
          type={isShown ? 'text' : 'password'}
          label="Password"
          name="password"
          value={values.password}
          onChange={(e: any) => onChange(e)}
        ></FormInput>

        <Checkbox
          id="checkbox"
          label="Show Password"
          checked={isShown}
          onChange={togglePassword}
        ></Checkbox>

        <Button type="submit">Login</Button>
      </form>
    </>
  );
};

export default SignInForm;
