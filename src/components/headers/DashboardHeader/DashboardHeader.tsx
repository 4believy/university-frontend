import styles from './DashboardHeader.module.scss';
import notification from '../../../assets/icons/notification.svg';

interface Props {
  title: string;
  avatar: string;
}

const DashboardHeader = ({ title, avatar }: Props) => {
  return (
    <div className={styles.header}>
      <h5 className={styles.header__title}>{title}</h5>
      <div className={styles.profile}>
        <img
          className={styles.profile__notification}
          src={notification}
          alt="notification icon"
        />
        <img className={styles.profile__avatar} src={avatar} alt="avatar" />
      </div>
    </div>
  );
};

export default DashboardHeader;
