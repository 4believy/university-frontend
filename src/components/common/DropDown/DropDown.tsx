import styles from './DropDown.module.scss';

const DropDown = () => {
  return (
    <div className={styles.dropdown}>
      <label className={styles.dropdown__label} htmlFor="sort">
        Sort by
      </label>

      <select className={styles.dropdown__select} name="cars" id="sort">
        <option value="all">All</option>
        <option value="a-z">A-Z</option>
        <option value="z-a">Z-A</option>
        <option value="newest">Newest first</option>
        <option value="latest">Latest First</option>
      </select>
    </div>
  );
};

export default DropDown;
