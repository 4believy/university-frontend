import Sidebar from '../../components/sidebar/Sidebar';
import DivFlex from '../../components/utils/DivFlex';
import LectorsLayout from '../../components/layouts/LectorsLayout';

const Lectors = () => {
  return (
    <DivFlex>
      <Sidebar></Sidebar>
      <LectorsLayout></LectorsLayout>
    </DivFlex>
  );
};

export default Lectors;
