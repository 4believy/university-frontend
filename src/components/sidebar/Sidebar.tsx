import styles from './Sidebar.module.scss';
import sidebarLogo from '../../assets/icons/sidebar-logo.svg';
import LogOutIcon from '../../assets/icons/tab/logout.svg';
import { useEffect, useState } from 'react';
import SidebarTab from './SidebarTab/SidebarTab';
import { sidebarNavItems } from './SidebarNavItems';
import { useDispatch } from 'react-redux';
import { logOut } from '../../redux/slices/authSlice';

const Sidebar = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    const curPath = window.location.pathname.split('/')[1];
    const activeItem = sidebarNavItems.findIndex(
      (item) => item.to === '/' + curPath,
    );
    setActiveIndex(activeItem);
  }, []);

  const dispatch = useDispatch();

  const LogOut = (): {
    payload: undefined;
    type: 'auth/logOut';
  } => dispatch(logOut());

  return (
    <div className={styles.sidebar}>
      <div className={styles.sidebar__header}>
        <img className={styles.sidebar__logo} src={sidebarLogo} alt="logo" />
      </div>

      <div className={styles.sidebar__content}>
        {sidebarNavItems.map((item, index) => (
          <SidebarTab
            navigateTo={item.to}
            key={index}
            icon={item.icon}
            text={item.text}
            active={activeIndex === index}
          ></SidebarTab>
        ))}
      </div>

      <div className={styles.sidebar__footer}>
        <SidebarTab
          navigateTo="/sign-in"
          text="Log out"
          icon={LogOutIcon}
          active={false}
          onClick={LogOut}
        ></SidebarTab>
      </div>
    </div>
  );
};

export default Sidebar;
