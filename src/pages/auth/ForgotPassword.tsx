import ForgotPasswordForm from '../../components/forms/AuthForms/ForgotPasswordForm';
import LinkComponent from '../../components/common/LinkComponent/LinkComponent';
import AuthPagesSnippet from '../../components/headers/AuthHeader/AuthHeader';

const ForgotPassword = () => {
  return (
    <AuthPagesSnippet title="Forgot Password">
      <ForgotPasswordForm></ForgotPasswordForm>
      <LinkComponent navigateTo="/sign-in">Cancel</LinkComponent>
    </AuthPagesSnippet>
  );
};

export default ForgotPassword;
