import AuthPagesSnippet from '../../components/headers/AuthHeader/AuthHeader';
import SignUpForm from '../../components/forms/AuthForms/SignUpForm';
import LinkComponent from '../../components/common/LinkComponent/LinkComponent';

const SignUp = () => {
  return (
    <AuthPagesSnippet title="Register your account">
      <SignUpForm></SignUpForm>
      <LinkComponent navigateTo="/sign-in">Sign in</LinkComponent>
    </AuthPagesSnippet>
  );
};

export default SignUp;
