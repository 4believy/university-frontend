import { Routes, Route, Outlet, Navigate } from 'react-router-dom';
import SignIn from './pages/auth/SignIn';
import SignUp from './pages/auth/SignUp';
import ForgotPassword from './pages/auth/ForgotPassword';
import ResetPassword from './pages/auth/ResetPassword';
import Home from './pages/Home';
import Dashboard from './pages/dashboard/Dashboard';
import Courses from './pages/dashboard/Courses';
import Lectors from './pages/dashboard/Lectors';
import Groups from './pages/dashboard/Groups';
import Students from './pages/dashboard/Students';
import { useSelector } from 'react-redux';
import { IAuthInitialState } from './redux/slices/authSlice';

interface IStore {
  auth: IAuthInitialState;
}

function App() {
  const getAccessToken = (store: IStore): string | null =>
    store.auth.accessToken;

  const accessToken = useSelector(getAccessToken);

  const PrivateRoutes = () => {
    return <>{accessToken ? <Outlet /> : <Navigate to="/sign-in" />}</>;
  };

  const RestrictedRoutes = () => {
    return <>{!accessToken ? <Outlet /> : <Navigate to="/dashboard" />}</>;
  };

  return (
    <>
      <Routes>
        <Route element={<RestrictedRoutes />}>
          <Route path="/" element={<Home />} />
          <Route path="/sign-in" element={<SignIn />} />
          <Route path="/sign-up" element={<SignUp />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/reset-password" element={<ResetPassword />} />
          <Route path="*" element={<Home />} />
        </Route>

        <Route element={<PrivateRoutes />}>
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/lectors" element={<Lectors />} />
          <Route path="/groups" element={<Groups />} />
          <Route path="/students" element={<Students />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
