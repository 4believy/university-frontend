import Sidebar from '../../components/sidebar/Sidebar';
import DivFlex from '../../components/utils/DivFlex';
import StudentsLayout from '../../components/layouts/StudentsLayout';

const Students = () => {
  return (
    <DivFlex>
      <Sidebar></Sidebar>
      <StudentsLayout></StudentsLayout>
    </DivFlex>
  );
};

export default Students;
