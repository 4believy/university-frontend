import { ReactNode } from 'react';
import styles from './DivFlex.module.scss';

interface Props {
  children: ReactNode;
}

const DivFlex = ({ children }: Props) => {
  return <div className={styles.divFlex}>{children}</div>;
};

export default DivFlex;
