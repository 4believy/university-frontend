import LinkComponent from '../components/common/LinkComponent/LinkComponent';

const Home = () => {
  return (
    <div>
      <LinkComponent navigateTo="/sign-in">Sign-in</LinkComponent>
      <LinkComponent navigateTo="/sign-up">Sign-up</LinkComponent>
      <LinkComponent navigateTo="/forgot-password">
        Forgot Password
      </LinkComponent>
      <LinkComponent navigateTo="/reset-password">Reset Password</LinkComponent>
      <LinkComponent navigateTo="/dashboard">DashBoard</LinkComponent>
    </div>
  );
};

export default Home;
