import { Link } from 'react-router-dom';
import styles from './SidebarTab.module.scss';

interface Props {
  navigateTo?: string;
  icon: string;
  text: string;
  active: boolean;
  onClick?: any;
}

const SidebarTab = ({
  navigateTo = '/dashboard',
  icon,
  text,
  active,
  onClick,
}: Props) => {
  return (
    <div
      className={`${styles.tab__wrapper} ${
        active ? styles.tab_active : styles.tab_inactive
      }`}
    >
      <Link onClick={onClick} className={styles.tab} to={navigateTo}>
        <img className={styles.tab__icon} src={icon} alt={`${text} icon`} />
        <p className={styles.tab__text}>{text}</p>
      </Link>
    </div>
  );
};

export default SidebarTab;
