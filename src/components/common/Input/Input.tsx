import React from 'react';
import {
  FieldErrors,
  FieldValues,
  UseFormRegisterReturn,
} from 'react-hook-form';

interface Props {
  id: string;
  type: string;
  name: string;
  label: string;
  value: string;
  placeholder?: string;
  register: UseFormRegisterReturn<string>;
  errors: FieldErrors<FieldValues>;
}

const Input = () => {
  return <div>Input</div>;
};

export default Input;
