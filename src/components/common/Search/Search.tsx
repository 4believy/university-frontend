import styles from './Search.module.scss';
import searchIcon from '../../../assets/icons//search-icon.svg';

const Search = () => {
  return (
    <div className={styles.search__wrapper}>
      <img className={styles.search__icon} src={searchIcon} alt="search icon" />
      <input className={styles.search__input} placeholder="Search"></input>
    </div>
  );
};

export default Search;
