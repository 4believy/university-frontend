import Header from '../headers/DashboardHeader/DashboardHeader';
import styles from './DashboardLayout.module.scss';
import avatar from '../../assets/avatar1.png';
import DropDown from '../common/DropDown/DropDown';
import Search from '../common/Search/Search';
import DashboardButton from '../common/DashboardButton/DashboardButton';

const StudentsLayout = () => {
  return (
    <div className={styles.dashboard}>
      <Header title="Students" avatar={avatar}></Header>
      <div className={styles.controlPanel}>
        <DropDown></DropDown>
        <Search></Search>
        <DashboardButton>Add new student</DashboardButton>
      </div>
    </div>
  );
};

export default StudentsLayout;
