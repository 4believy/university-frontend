import Sidebar from '../../components/sidebar/Sidebar';
import DivFlex from '../../components/utils/DivFlex';
import CoursesLayout from '../../components/layouts/CoursesLayout';

const Courses = () => {
  return (
    <DivFlex>
      <Sidebar></Sidebar>
      <CoursesLayout></CoursesLayout>
    </DivFlex>
  );
};

export default Courses;
