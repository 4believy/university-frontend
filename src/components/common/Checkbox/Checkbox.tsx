import styles from './Checkbox.module.scss';

interface Props {
  id: string;
  label: string;
  checked?: boolean;
  onChange?: () => void;
}

const Checkbox = ({ id, label, checked, onChange }: Props) => {
  return (
    <div className={styles.checkboxComponent__wrapper}>
      <input id={id} type="checkbox" checked={checked} onChange={onChange} />

      <label htmlFor={id}>{label}</label>
    </div>
  );
};

export default Checkbox;
