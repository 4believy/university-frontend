import Sidebar from '../../components/sidebar/Sidebar';
import DivFlex from '../../components/utils/DivFlex';
import GroupsLayout from '../../components/layouts/GroupsLayout';

const Groups = () => {
  return (
    <DivFlex>
      <Sidebar></Sidebar>
      <GroupsLayout></GroupsLayout>
    </DivFlex>
  );
};

export default Groups;
