import Sidebar from '../../components/sidebar/Sidebar';
import DivFlex from '../../components/utils/DivFlex';
import DashboardLayout from '../../components/layouts/DashboardLayout';

const Dashboard = () => {
  return (
    <DivFlex>
      <Sidebar></Sidebar>
      <DashboardLayout></DashboardLayout>
    </DivFlex>
  );
};

export default Dashboard;
