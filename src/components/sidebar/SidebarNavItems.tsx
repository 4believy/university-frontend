import dashboard from '../../assets/icons/tab/dashboard.svg';
import courses from '../../assets/icons/tab/courses.svg';
import lectors from '../../assets/icons/tab/lectors.svg';
import groups from '../../assets/icons/tab/groups.svg';
import students from '../../assets/icons/tab/students.svg';

export const sidebarNavItems = [
  {
    text: 'Dashboard',
    icon: dashboard,
    to: '/dashboard',
  },
  {
    text: 'Courses',
    icon: courses,
    to: '/courses',
  },
  {
    text: 'Lectors',
    icon: lectors,
    to: '/lectors',
  },
  {
    text: 'Groups',
    icon: groups,
    to: '/groups',
  },
  {
    text: 'Students',
    icon: students,
    to: '/students',
  },
];
