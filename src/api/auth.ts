import axios, { AxiosResponse } from 'axios';
import { ISignRequest } from '../utils/types/sign.interface';
import { ISignResponse } from '../utils/types/sign.interface';

const axiosInstance = axios.create({
  baseURL: 'http://localhost:3010',
});

export const setToken = (token: string | undefined): string | undefined => {
  if (token) {
    return (axiosInstance.defaults.headers[
      'Authorization'
    ] = `Bearer ${token}`);
  }

  axiosInstance.defaults.headers['Authorization'] = '';
};

export const signUp = async (
  payload: ISignRequest,
): Promise<AxiosResponse<ISignResponse>> =>
  axiosInstance.post('/auth/sign-up', payload);

export const signIn = async (
  payload: ISignRequest,
): Promise<AxiosResponse<ISignResponse>> => {
  const result = await axiosInstance.post('/auth/sign-in', payload);

  setToken(result.data.accessToken);

  return result;
};
