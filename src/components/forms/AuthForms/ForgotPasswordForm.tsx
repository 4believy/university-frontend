import { useState } from 'react';
import Button from '../../common/Button/Button';
import styles from './AuthForms.module.scss';
import FormInput from '../../common/FormInput/FormInput';

const ForgotPasswordForm = () => {
  const [values, setValues] = useState({
    email: '',
  });
  const onChange = (e: any) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  const onSubmit = (e: any): void => {
    e.preventDefault();
  };

  return (
    <>
      <form noValidate className={styles.authForm} onSubmit={onSubmit}>
        <p className={styles.authForm__text}>
          Don't worry, happens to the best of us. Enter the email address
          associated with your account and we'll send you a link to reset.
        </p>
        <FormInput
          id="email"
          type="email"
          label="Email"
          name="email"
          value={values.email}
          onChange={(e: any) => onChange(e)}
          placeholder="name@mail.com"
        ></FormInput>

        <Button type="submit">Reset</Button>
      </form>
    </>
  );
};

export default ForgotPasswordForm;
