import { useState } from 'react';
import styles from './AuthForms.module.scss';
import Checkbox from '../../common/Checkbox/Checkbox';
import Button from '../../common/Button/Button';
import LinkComponent from '../../common/LinkComponent/LinkComponent';
import FormInput from '../../common/FormInput/FormInput';
import AuthPagesSnippet from '../../headers/AuthHeader/AuthHeader';

const ResetPasswordForm = () => {
  const [values, setValues] = useState({
    password: '',
    confirmPassword: '',
  });

  const onChange = (e: any) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };
  const [isShown, setIsSHown] = useState(false);
  const [currentPage, setCurrentPage] = useState(true);

  const togglePassword = () => {
    setIsSHown((isShown) => !isShown);
  };

  const changePage = () => {
    setCurrentPage(false);
  };

  const onSubmit = (e: any): void => {
    e.preventDefault();
  };

  return (
    <>
      {currentPage ? (
        <>
          <AuthPagesSnippet title={'Reset Your Password'}>
            <form noValidate className={styles.authForm} onSubmit={onSubmit}>
              <FormInput
                id="password"
                type={isShown ? 'text' : 'password'}
                label="New Password"
                name="password"
                value={values.password}
                onChange={(e: any) => onChange(e)}
              ></FormInput>

              <FormInput
                id="confirmPassword"
                type={isShown ? 'text' : 'password'}
                label="Confirm password"
                name="confirmPassword"
                value={values.confirmPassword}
                onChange={(e: any) => onChange(e)}
              ></FormInput>

              <Checkbox
                id="checkbox"
                label="Show Password"
                checked={isShown}
                onChange={togglePassword}
              ></Checkbox>

              <Button onClick={changePage} type="submit">
                Reset
              </Button>
            </form>
          </AuthPagesSnippet>
        </>
      ) : (
        <>
          <AuthPagesSnippet title={'Password Changed'}>
            <div className={styles.authContent}>
              <p className={styles.authContent__text}>
                You can use your new password to log into your account
              </p>
              <LinkComponent design="secondary" navigateTo="/sign-in">
                Log In
              </LinkComponent>
            </div>
          </AuthPagesSnippet>
        </>
      )}
    </>
  );
};

export default ResetPasswordForm;
